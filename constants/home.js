export const CAROUSEL = [
  "https://cdn.shopify.com/s/files/1/0003/8718/6741/files/Desktop_1560x600_2_94415426-5ebc-4fc1-b0e0-94afbec85c3f_1800x.jpg?v=1647506052",
  "https://cdn.shopify.com/s/files/1/0003/8718/6741/files/Desktop_1560x600_1_4063e82a-e3bb-40ef-b558-66c4c5dbb14d_1800x.jpg?v=1647505744",
  "https://cdn.shopify.com/s/files/1/0003/8718/6741/files/Desktop_1560x600_b00e7609-51e8-4215-ac55-f8f693128caa_1400x.jpg?v=1646121714",
];

export const PRODUCT = [
  {
    img: "https://cdn.shopify.com/s/files/1/0003/8718/6741/products/GS_The-Move_200x.jpg?v=1606972972",
    imgHover:
      "https://cdn.shopify.com/s/files/1/0003/8718/6741/p…ucts/Mercedes-BenzTHEMOVE19_300x.jpg?v=1606972987",
    status: "SALE",
    name: "Gift Set Mercedes-Benz The Move EDT",
    price: "1,393,000 VND",
    oldPrice: "1,990,000 VND",
  },
  {
    img: "https://cdn.shopify.com/s/files/1/0003/8718/6741/products/Select_acddf236-3d1f-4095-aee8-2daa773b2292_200x.jpg?v=1606821861",

    imgHover:
      "https://cdn.shopify.com/s/files/1/0003/8718/6741/p…ducts/Mercedes-BenzOntheGo3_300x.gif?v=1606821861",
    status: "SALE",
    name: "[NEW] Mercedes-Benz On The Go",
    price: "385,000 VND",
    oldPrice: "550,000 VND",
  },
  {
    img: "https://cdn.shopify.com/s/files/1/0003/8718/6741/products/Mercedes-Benz-for-men-edt-120ml-flanker_200x.jpg?v=1597630250",

    imgHover:
      "https://cdn.shopify.com/s/files/1/0003/8718/6741/p…U-DE-TOILETTE-FOR-MEN_120ml_300x.jpg?v=1597630250",
    status: "SALE",
    name: "Mercedes-Benz For Men EDT",
    price: "854,000 VND",
    oldPrice: "1,220,000 VND",
  },
  {
    img: "https://cdn.shopify.com/s/files/1/0003/8718/6741/products/NEW_-Kenzo-L_eau-Hyper-Wave-Pour-Homme-2020_200x.jpg?v=1599452039",
    imgHover:
      "https://cdn.shopify.com/s/files/1/0003/8718/6741/p…yper-Wave-for-him-100ml_box_300x.jpg?v=1599452039",
    status: "SALE",
    name: "Kenzo L'eau Hyper Wave Pour Homme 2020",
    price: "833,000 VND",
    oldPrice: "1,190,000 VND",
  },
  {
    img: "https://cdn.shopify.com/s/files/1/0003/8718/6741/products/Paco-Rabanne-Lady-Million-Lucky-Edp-0_200x.jpg?v=1597630093",
    imgHover:
      "https://cdn.shopify.com/s/files/1/0003/8718/6741/p…-Million-Lucky-Edp-80ml-box_300x.jpg?v=1597630093",
    status: "",
    name: "Paco Rabanne Lady Million Lucky EDP (For Women)",
    price: "1,120,000 VND",
    oldPrice: "1,600,000 VND",
  },
  {
    img: "https://cdn.shopify.com/s/files/1/0003/8718/6741/products/Carolina-Herrera-CH-Privee-Edp-80Ml_30d63545-95bb-42b6-b981-d2cb1b5cfebb_200x.jpg?v=1601883314",
    imgHover:
      "https://cdn.shopify.com/s/files/1/0003/8718/6741/p…23af-46af-afe3-386c7320085f_300x.jpg?v=1601883318",
    status: "",
    name: "Carolina Herrera CH Privée EDP (Christmas Collection)",
    price: "Từ 1,630,000 VND",
    oldPrice: "",
  },
];
