
function Product(props) {
    const { img,
    imgHover,
    status,
    name,
    price,
    oldPrice} = props.product;
    return (
      <div className="w-1/6 px-3 flex justify-center flex-wrap mb-20 ">
        <div className="relative">
          <img src={img} alt={name} className="rounded-md w-full"></img>
          {status === "SALE" ? (
            <div className="absolute top-1 right-[2%] bg-red-600 text-white  text-[10px] font-bold w-[35px] h-[35px] rounded-t-[50%] rounded-br-[50%] flex items-center justify-center">
              SALE
            </div>
          ) : (
            <div className="absolute top-1 left-[2%] text-yellow-400  text-[11px] font-bold bg-white px-2 py-1 text-center">
              ĐÃ HẾT HÀNG
            </div>
          )}
        </div>
        <div className="text-center mt-4 text-sm leading-loose">
          <p>{name}</p>
          <p>{price}</p>
          {/* text-decoration: line-through */}
          {oldPrice && (
            <span className="text-yellow-400 relative before:content-[''] before:absolute  before:top-[8px] before:left-[-6px] before:w-[calc(100%+1em)] before:h-[1.5px] before:bg-yellow-400">
              {oldPrice}
            </span>
          )}
        </div>
      </div>
    );
}

export default Product;