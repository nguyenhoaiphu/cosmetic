module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      keyframes: {
        "my-opacity": {
          "0%": {
            opacity: "0",
          },
          "100%": {
            opacity: "1",
          },
        },
        "hover-menu": {
          "0%": {
            width: "0px",
          },
          "100%": {
            width: "100%",
          },
        },
      },
      animation: {
        "my-opacity": "my-opacity 1s ease-out",
        "hover-menu": "hover-menu 1s ease-out",
      },
    },
  },
  plugins: [],
};
